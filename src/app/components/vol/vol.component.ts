import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Vol } from '../../models/vol.model';
import { MatIconModule } from '@angular/material/icon';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-vol',
  standalone: true,
  imports: [MatIconModule, CommonModule],
  templateUrl: './vol.component.html',
  styleUrls: ['./vol.component.scss']
})
export class VolComponent {
  @Input() vol!: Vol;
  @Input() type: 'decollages' | 'atterrissages' = 'decollages';
  @Output() selectVol = new EventEmitter<Vol>();

  onSelectVol() {
    console.log('Click sur le vol :', this.vol);
    this.selectVol.emit(this.vol);
  }

  getCompagnieLogo(compagnie: string): string {
    switch (compagnie) {
      case 'Air France':
        return 'assets/Air France Hop.png';
      case 'Air France Hop':
        return 'assets/Air France.png';
      case 'Transavia France':
        return 'assets/Transavia France.png';
      default:
        return '';
    }
  }
}