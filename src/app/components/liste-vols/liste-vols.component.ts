import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VolComponent } from '../vol/vol.component';
import { Vol } from '../../models/vol.model';

@Component({
  selector: 'app-liste-vols',
  standalone: true,
  imports: [CommonModule, VolComponent],
  templateUrl: './liste-vols.component.html',
  styleUrls: ['./liste-vols.component.scss']
})
export class ListeVolsComponent {
  @Input() vols: Vol[] = [];
  @Input() type: 'decollages' | 'atterrissages' = 'decollages';
  @Output() selectVol = new EventEmitter<Vol>();

  onSelectVol(vol: Vol) {
    console.log('Récéption click vol :', vol);
    this.selectVol.emit(vol);
  }
}
