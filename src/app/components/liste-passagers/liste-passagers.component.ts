import { Component, Input } from '@angular/core';
import { IPassager } from '../../models/passager.model';
import { PassagerComponent } from '../passager/passager.component';
import { CommonModule } from '@angular/common';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-liste-passagers',
  standalone: true,
  imports: [PassagerComponent, CommonModule, MatSlideToggleModule, FormsModule],
  templateUrl: './liste-passagers.component.html',
  styleUrls: ['./liste-passagers.component.scss']
})
export class ListePassagersComponent {
  @Input() passagers: IPassager[] = [];
  showPhotos: boolean = false;
}
