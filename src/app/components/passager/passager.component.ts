import { Component, Input } from '@angular/core';
import { IPassager } from '../../models/passager.model';
import { ClasseVolColorDirective } from '../../directives/classe-vol-color.directive';
import { LuggageWeightDirective } from '../../directives/luggage-weight.directive';
import { CommonModule } from '@angular/common';
import { MatTooltipModule } from '@angular/material/tooltip';

@Component({
  selector: 'app-passager',
  standalone: true,
  imports: [ClasseVolColorDirective, LuggageWeightDirective, CommonModule, MatTooltipModule],
  templateUrl: './passager.component.html',
  styleUrls: ['./passager.component.scss']
})
export class PassagerComponent {
  @Input() passager!: IPassager;
  @Input() showPhotos: boolean = false;
}
