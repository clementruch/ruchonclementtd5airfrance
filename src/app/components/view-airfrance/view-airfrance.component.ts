import { Component, OnInit } from '@angular/core';
import { FiltresComponent } from '../filtres/filtres.component';
import { ListeVolsComponent } from '../liste-vols/liste-vols.component';
import { ListePassagersComponent } from '../liste-passagers/liste-passagers.component';
import { VolService } from '../../services/vol.service';
import { IAeroport } from '../../models/aeroport.model';
import { Vol } from '../../models/vol.model';
import { IPassager } from '../../models/passager.model';
import { PassagerService } from '../../services/passager.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-airfrance',
  standalone: true,
  imports: [FiltresComponent, ListeVolsComponent, ListePassagersComponent],
  templateUrl: './view-airfrance.component.html',
  styleUrls: ['./view-airfrance.component.scss']
})
export class ViewAirFranceComponent implements OnInit {
  vols: Vol[] = [];
  passagers: IPassager[] = [];
  type: 'decollages' | 'atterrissages' = 'decollages';

  constructor(private volService: VolService, private passagerService: PassagerService, private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(data => {
      this.type = data['type'];
    });
  }

  onFiltresChange(filtres: { aeroport: IAeroport, dateDebut: Date, dateFin: Date }) {
    const { aeroport, dateDebut, dateFin } = filtres;
    const debutSeconds = Math.floor(dateDebut.getTime() / 1000);
    const finSeconds = Math.floor(dateFin.getTime() / 1000);

    if (this.type === 'decollages') {
      this.volService.getVolsDepart(aeroport.icao, debutSeconds, finSeconds).subscribe(vols => {
        this.vols = vols;
      });
    } else if (this.type === 'atterrissages') {
      this.volService.getVolsArrivee(aeroport.icao, debutSeconds, finSeconds).subscribe(vols => {
        this.vols = vols;
      })
    }
  }

  onSelectVol(vol: Vol) {
    console.log('Récéption passagers après click', vol);
    this.passagerService.getPassagers(vol.icao).subscribe((passagers: IPassager[]) => {
      console.log('Passagers récupérés:', passagers);
      this.passagers = passagers;
    });
  }
}
