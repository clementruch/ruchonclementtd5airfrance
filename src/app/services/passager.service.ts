import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IPassagerDto, Passager } from '../models/passager.model';

@Injectable({
  providedIn: 'root'
})
export class PassagerService {

  constructor(private http: HttpClient) {}

  getPassagers(icao: string): Observable<Passager[]> {
    return this.http.get<{ results: IPassagerDto[] }>('https://randomuser.me/api/?results=20&inc=name,picture,email&seed=${icao}').pipe(
      map(response => response.results
        .map(dto => new Passager(dto)))
    );
  }
}
