import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appClasseVolColor]',
  standalone: true
})
export class ClasseVolColorDirective implements OnInit {
  @Input() appClasseVolColor!: string;

  constructor(private el: ElementRef) { }

  ngOnInit() {
    this.setColor();
  }

  private setColor() {
    switch (this.appClasseVolColor) {
      case 'BUSINESS':
        this.el.nativeElement.style.color = 'red';
        break;
      case 'PREMIUM':
        this.el.nativeElement.style.color = 'green';
        break;
      case 'STANDARD':
        this.el.nativeElement.style.color = 'blue';
        break;
      default:
        this.el.nativeElement.style.color = 'black';
        break;
    }
  }
}