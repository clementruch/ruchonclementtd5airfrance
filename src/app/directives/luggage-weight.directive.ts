import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appLuggageWeight]',
  standalone: true
})
export class LuggageWeightDirective implements OnInit{
  @Input() classeVol!: string;
  @Input() appLuggageWeight!: number;

  constructor(private el: ElementRef) { }

  ngOnInit() {
    this.weightLimit();
  }

  private weightLimit() {
    switch (this.classeVol) {
      case 'BUSINESS':
        if (this.appLuggageWeight > 2) {
          this.el.nativeElement.style.backgroundColor  = 'red';
        }
        break;
      case 'PREMIUM':
        if (this.appLuggageWeight > 3) {
          this.el.nativeElement.style.backgroundColor  = 'red';
        }
        break;
      case 'STANDARD':
        if (this.appLuggageWeight > 1) {
          this.el.nativeElement.style.backgroundColor  = 'red';
        }
        break;
    }
  }
}
